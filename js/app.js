"use strict";

const numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?", "");

const personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false,
};
    // 1 способ написать цикл
for(let i = 0; i < 2; i++){
    const lastFilm = prompt("Один из последних просмотренных фильмов?", "");
    const resFilm = prompt("На сколько оцените его?", "");
    if(lastFilm != '' && resFilm != '' && lastFilm != null & resFilm != null && lastFilm.length < 50){
        personalMovieDB.movies[lastFilm] = resFilm;
    } else{
        i--;
    };  
};
    //

    // 2 способ переписать цикл
// let i = 0;
// while(i < 2){
//     ++i;  
//     const lastFilm = prompt("Один из последних просмотренных фильмов?", "");
//     const resFilm = prompt("На сколько оцените его?", "");
//     if(lastFilm != '' && resFilm != '' && lastFilm != null & resFilm != null && lastFilm.length < 50){
//         personalMovieDB.movies[lastFilm] = resFilm;
//     } else{
//         i--;
//     };
// };
    //

    //  3 способ переписать цикл
// let i = 0;
// do{
//     i++;
//     const lastFilm = prompt("Один из последних просмотренных фильмов?", "");
//     const resFilm = prompt("На сколько оцените его?", "");
//     if(lastFilm != '' && resFilm != '' && lastFilm != null & resFilm != null && lastFilm.length < 50){
//         personalMovieDB.movies[lastFilm] = resFilm;
//     } else{
//         i--;
//     };
// } while(i < 2);
    //
    
if(personalMovieDB.count < 10){
    console.log("Просмотрено довольно мало фильмов");
} else if(personalMovieDB.count >= 10 && personalMovieDB.count < 30){
    console.log("Вы классический зритель");
} else if(personalMovieDB.count >= 30){
    console.log("Вы киноман");
} else{
    console.log("Произошла ошибка");
}

console.log(personalMovieDB)


